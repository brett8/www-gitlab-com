

<table>
  <tr><td><strong>Key:</strong></td>
  <td bgcolor="#e1f7d2">Leads</td>
  <td bgcolor="#f7ddd2">Behind</td>
  <td>On Par</td>
  <td bgcolor="yellow">Separate Product</td></tr>
</table>


<table>
  <tr>
   <td></td>
   <td><strong>GitLab</strong> </td>
   <td><strong>Azure DevOps</strong></td>
   <td><strong>Notes</strong></td>
  </tr>
  <tr>
   <td>Manage</td>
   <td bgcolor="#f7ddd2"><a href="https://about.gitlab.com/stages-devops-lifecycle/manage/">GitLab</a></td>
   <td> (dashboards)</td>
   <td>GitLab is weaker in dashboards and reporting</td>
  </tr>
  <tr>
   <td>Plan</td>
   <td><a href="https://about.gitlab.com/stages-devops-lifecycle/plan/">GitLab</a></td>
   <td>
    <ul>
      <li><a href="https://azure.microsoft.com/en-us/services/devops/boards/">Azure Boards</a></li>
      <li><span class="highlight-yellow"><a href="https://products.office.com/en-us/project/project-portfolio-management">MS Project Portfolio Management</a>+</span></li>
    </ul>
   </td>
   <td>+ MS PM separate product</td>
  </tr>
  <tr>
   <td>Create</td>
   <td><a href="https://about.gitlab.com/stages-devops-lifecycle/create/">GitLab</a> </td>
   <td>
      <ul>
      <li><a href="https://azure.microsoft.com/en-us/services/devops/repos/">Azure
      Repos</a></li>
      <li><span class="highlight-green"><a href="https://github.com/">GitHub</a></span></li>
      </ul>
   </td>
   <td>GH gives MS more reach into developer organizations</td>
  </tr>
  <tr>
   <td>Verify</td>
   <td bgcolor="#e1f7d2"><a href="https://about.gitlab.com/stages-devops-lifecycle/verify/">GitLab</a> </td>
   <td>
    <ul>
      <li><a href="https://azure.microsoft.com/en-us/services/devops/pipelines/">Azure Pipelines</a></li>
      <li><span class="highlight-green"><a href="https://azure.microsoft.com/en-us/services/devops/test-plans/">Azure Test Plans</a></span></li>
    </ul>
   </td>
   <td>GL better pipelines, Azure has test plans and creation</td>
  </tr>
  <tr>
   <td>Secure</td>
   <td bgcolor="#e1f7d2"><a href="https://about.gitlab.com/stages-devops-lifecycle/secure/">GitLab</a>
   </td>
   <td bgcolor="#f7ddd2"> <em>(<a href="https://azure.microsoft.com/en-us/products/devops-tool-integrations/">integrations</a>)</em></td>
   <td> </td>
  </tr>
  <tr>
   <td>Package</td>
   <td><a href="https://about.gitlab.com/stages-devops-lifecycle/package/">GitLab</a></td>
   <td bgcolor="#e1f7d2">
      <ul>
        <li><a href="https://azure.microsoft.com/en-us/services/devops/artifacts/">Azure Artifacts</a></li>
        <li><span class="highlight-yellow"><a href="https://azure.microsoft.com/en-us/services/container-registry/">Azure Container Registry</a></span> + </li>
      </ul>
   </td>
   <td>Azure packaging and registry more mature <br>+Container Reg separate product</td>
  </tr>
  <tr>
   <td>Release</td>
   <td bgcolor="#e1f7d2"><a href="https://about.gitlab.com/stages-devops-lifecycle/release/">GitLab</a></td>
   <td>
    <ul>
      <li><a href="https://azure.microsoft.com/en-us/services/devops/pipelines/">Azure Pipelines</a></li>
    </ul>
   </td>
   <td>GL more advanced capabilities</td>
  </tr>
  <tr>
   <td>Configure
   </td>
   <td bgcolor="#e1f7d2"><a href="https://about.gitlab.com/stages-devops-lifecycle/configure/">GitLab</a></td>
   <td bgcolor="#f7ddd2"></td>
   <td>GL Auto DevOps</td>
  </tr>
  <tr>
   <td>Monitor</td>
   <td><a href="https://about.gitlab.com/stages-devops-lifecycle/monitor/">GitLab</a></td>
   <td>
    <ul>
    <li><span class="highlight-yellow"><a href="https://azure.microsoft.com/en-us/services/monitor/">Azure
    Monitor</a></span> + </li>
    </ul>
   </td>
   <td>+Azure more complete, but +$ and not integrated (sep product) </td>
  </tr>
  <tr>
   <td>Defend </td>
   <td><a href="https://about.gitlab.com/stages-devops-lifecycle/defend/">GitLab</a> (coming) </td>
   <td bgcolor="#e1f7d2">
    <ul>
      <li><span class="highlight-yellow"><a href="https://azure.microsoft.com/en-us/services/security-center/?&OCID=AID719825_SEM_uP2AeOiW&lnkd=Google_Azure_Brand&gclid=EAIaIQobChMIjbHKpOvM4AIVWR-tBh198Q_xEAAYBCAAEgJ3hPD_BwE">Azure Security Center</a> </span> + </li>
    </ul>
   </td>
   <td>+ separate service, but well established </td>
  </tr>
</table>
<table>
  <tr><td><strong>Legend:</strong></td>
  <td bgcolor=>Leads</td>
  <td bgcolor="#f7ddd2">Behind</td>
  <td>On Par</td>
  <td bgcolor="yellow">Separate Product</td></tr>
</table>
